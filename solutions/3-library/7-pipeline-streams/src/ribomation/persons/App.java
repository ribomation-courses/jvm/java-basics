package ribomation.persons;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class App {
    public static void main(String[] args) throws Exception {
        new App().run("./data/persons.csv");
    }

    void run(String filename) throws IOException {
        var path = Path.of(filename);
        if (!Files.isReadable(path)) {
            throw new RuntimeException("cannot open " + filename);
        }

        var persons =
        Files.lines(path)
                .skip(1)
                .map(line -> line.split(","))
                .map(arr -> new Person(arr[0], arr[1], arr[2], arr[3]))
                .filter(Person::isFemale)
                .filter(p -> p.getPostCode() < 20_000)
                .filter(p -> (30 <= p.getAge()) && (p.getAge() <= 40))
//                .forEach(System.out::println)
//                .collect(Collectors.toList())
                .collect(Collectors.toCollection(TreeSet::new))
        ;

        System.out.printf("found %d persons%n", persons.size());
        persons.forEach(System.out::println);
    }
}
