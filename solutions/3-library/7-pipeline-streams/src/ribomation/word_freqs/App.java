package ribomation.word_freqs;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Random;
import java.util.regex.Pattern;

import static java.lang.String.format;
import static java.lang.System.lineSeparator;
import static java.lang.System.nanoTime;
import static java.nio.file.Files.*;
import static java.util.stream.Collectors.*;

public class App {
    public static void main(String[] args) throws IOException {
//        new App().run("./data/musketeers.txt", "./out/musketeers.html");
        new App().run("./data/shakespeare.txt", "./out/shakespeare.html");
    }

    void run(String infile, String outfile) throws IOException {
        var srcPath = Path.of(infile);
        if (!isReadable(srcPath)) {
            throw new RuntimeException("cannot open " + infile);
        }

        var startTime   = nanoTime();
        var wordPattern = Pattern.compile("[^a-zA-Z]+");
        var freqs = lines(srcPath)
                .flatMap(wordPattern::splitAsStream)
                .filter(word -> word.length() > 4)
                .collect(groupingBy(String::toLowerCase, counting()));

        var result = freqs.entrySet().stream()
                .sorted((lhs, rhs) -> (int) (rhs.getValue() - lhs.getValue()))
                .limit(100)
                .collect(toList());

        var maxFont  = 150;
        var minFont  = 15;
        var maxFreqs = result.get(0).getValue();
        var minFreqs = result.get(result.size() - 1).getValue();
        var scale    = (double) (maxFont - minFont) / (double) (maxFreqs - minFreqs);

        var prefix = "<html><head>" +
                "<title>Words Freqs</title>" +
                "</head><body>";
        var suffix = "</body></html>";
        var r      = new Random();
        var html =
                result.stream()
                        .map(wf -> {
                            var word = wf.getKey();
                            var freq = wf.getValue();
                            var size = scale * freq + minFont;
                            var color = format("#%02x%02x%02x",
                                    r.nextInt(256), r.nextInt(256), r.nextInt(256)
                            );
                            return format("<span style='font-size: %dpx; color: %s;'>%s</span>",
                                    (int) size, color, word);
                        })
                        .sorted((_l, _r) -> r.nextBoolean() ? -1 : +1)
                        .collect(joining(lineSeparator(), prefix, suffix));

        writeString(Path.of(outfile), html);
        var endTime = nanoTime();
        System.out.printf("written %s%n", outfile);
        System.out.printf("elapsed %.3f seconds%n", (endTime - startTime) * 1E-9);
    }
}
