package ribomation.products;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class ProductGenerator {
    private Random r = new Random();

    public List<Product> nextProductList(int n) {
        var lst = new ArrayList<Product>();
        while (--n > 0) lst.add(nextProduct());
        return lst;
    }

    public Product nextProduct() {
        return new Product(nextName(), nextPrice(), nextCount());
    }

    private int nextCount() {
        return r.nextInt(11);
    }

    private double nextPrice() {
        //gaussian value G and then return P=250*G + 1000 (ensure P > 10)
        double P = 0;
        do {
            double G = r.nextGaussian();
            P = 250 * G + 1000;
        } while (P <= 10);
        return P;
    }

    private String nextName() {
        //list fake product names and pick a random name
        return names.get(r.nextInt(names.size()));
    }

    private List<String> names = Arrays.asList(
            "Volvo", "SAAB", "Ford", "Mazda", "Tesla",
            "Jeep", "BMW", "Audi", "LandRover", "Toyota"
    );
}
