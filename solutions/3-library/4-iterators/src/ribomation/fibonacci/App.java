package ribomation.fibonacci;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.run(10);
        app.run(42);
        app.run(90);
        app.run2(10);
    }

    void run(int count) {
        System.out.printf("fib(%d): ", count);
        for (var f : new Fibonacci(count)) System.out.printf("%d ", f);
        System.out.println();
    }

    void run2(int count) {
        System.out.printf("-- fib(%d) --%n", count);
        new Fibonacci(count).forEach(System.out::println);
    }
}
