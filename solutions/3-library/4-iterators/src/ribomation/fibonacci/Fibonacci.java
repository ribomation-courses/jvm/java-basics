package ribomation.fibonacci;

import java.util.Iterator;
import java.util.function.Consumer;

public class Fibonacci implements Iterable<Long> {
    private final int max;

    public Fibonacci(int max) {
        this.max = max;
    }

    @Override
    public Iterator<Long> iterator() {
        return new FibIter();
    }

    @Override
    public void forEach(Consumer<? super Long> consumer) {
        for (Long fib : this) consumer.accept(fib);
    }

    private class FibIter implements Iterator<Long> {
        long fib_n_minus_2 = 0;
        long fib_n_minus_1 = 1;
        int  count         = 0;

        @Override
        public boolean hasNext() {
            return count < max;
        }

        @Override
        public Long next() {
            long fib = fib_n_minus_2 + fib_n_minus_1;
            fib_n_minus_2 = fib_n_minus_1;
            fib_n_minus_1 = fib;
            ++count;
            return fib_n_minus_2;
        }
    }

}
