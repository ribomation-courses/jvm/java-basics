package ribomation;

import java.io.*;
import java.util.Objects;

public class Product implements Comparable<Product>, /*Serializable*/ Externalizable {
    private String name;
    private float  price;
    private int    count;

    public Product() {
    }

    public Product(String name, String price, String count) {
        this(name, Float.parseFloat(price), Integer.parseInt(count));
    }

    public Product(String name, float price, int count) {
        this.name = name;
        this.price = price;
        this.count = count;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeUTF(name);
        out.writeFloat(price);
        out.writeInt(count);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        name = in.readUTF();
        price = in.readFloat();
        count = in.readInt();
    }

    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }

    public int getCount() {
        return count;
    }

    @Override
    public String toString() {
        return String.format("Product{%s, %.2f kr, %d left}", name, price, count);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Float.compare(product.getPrice(), getPrice()) == 0 &&
                getCount() == product.getCount() &&
                getName().equals(product.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, price, count);
    }

    @Override
    public int compareTo(Product o) {
        return this.name.compareTo(o.name);
    }
}
