package ribomation;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import static java.nio.file.Files.*;

public class App {
    public static void main(String[] args) throws Exception {
        var app = new App();
        app.runAll("./data/products.csv");
    }

    void runAll(String filename) throws IOException, ClassNotFoundException {
        if (!isReadable(Path.of(filename))) {
            throw new RuntimeException("cannot open file: " + filename);
        }

        var products = new ProductRepo().load(filename).getProducts();
        System.out.printf("loaded %d objects%n", products.size());

        var tmp = createDirectory(Path.of("out/data"));
        var txt = storeText(products, tmp);
        var ser = storeSerialized(products, tmp);
        var zip = storeCompressed(products, tmp);

        System.out.printf("%s (%.1f%%) %n", txt, 100.0);
        System.out.printf("%s (%.1f%%) %n", ser, 100.0 * ser.size / txt.size);
        System.out.printf("%s (%.1f%%) %n", zip, 100.0 * zip.size / txt.size);

        deleteIfExists(txt.file);
        deleteIfExists(ser.file);
        deleteIfExists(zip.file);
        deleteIfExists(tmp);
    }

    Data storeText(List<Product> products, Path dir) throws IOException {
        var file = Files.createTempFile(dir, "list", ".txt");
        Files.writeString(file, products.toString());
        return new Data(file);
    }

    Data storeSerialized(List<Product> products, Path dir) throws IOException, ClassNotFoundException {
        var file = Files.createTempFile(dir, "list", ".ser");
        store(products, newOutputStream(file), newInputStream(file));
        return new Data(file);
    }

    Data storeCompressed(List<Product> products, Path dir) throws IOException, ClassNotFoundException {
        var file = Files.createTempFile(dir, "list", ".ser.gz");
        store(products, new GZIPOutputStream(newOutputStream(file)), new GZIPInputStream(newInputStream(file)));
        return new Data(file);
    }

    void store(List<Product> payload, OutputStream os, InputStream is) throws IOException, ClassNotFoundException {
        var oos = new ObjectOutputStream(os);
        try (oos) {
            oos.writeObject(payload);
        }
        var ois = new ObjectInputStream(is);
        try (ois) {
            var restored = (List<Product>) ois.readObject();
            if (!payload.equals(restored)) {
                throw new RuntimeException("ooooops, not equals");
            }
        }
    }

}
