package ribomation;

import java.io.IOException;
import java.nio.file.Path;

import static java.nio.file.Files.size;

public class Data {
    Path file;
    long size;

    Data(Path file) throws IOException {
        this.file = file;
        this.size = size(file);
    }

    @Override
    public String toString() {
        return String.format("%s: %.1f KB", file, size / 1024.0);
    }
}
