package ribomation;

public class EmailChecker {
    public static boolean isEmail(String txt) {
        if (txt == null) return false;
        if (txt.isBlank()) return false;

        var halves = txt.split("@");
        if (halves.length != 2) return false;

        return checkName(halves[0]) && checkDomain(halves[1]);
    }

    private static boolean checkName(String name) {
        if (name.isBlank()) return false;
        if (!Character.isLetter(name.charAt(0))) return false;
        if (name.charAt(name.length() - 1) == '.') return false;
        if (!name.matches("[a-zA-Z0-9.-]+")) return false;

        return true;
    }

    private static boolean checkDomain(String domain) {
        if (domain.isBlank()) return false;
        if (!Character.isLetter(domain.charAt(0))) return false;
        if (domain.charAt(domain.length() - 1) == '.') return false;
        if (!domain.matches("[a-zA-Z.]+")) return false;

        var tldIdx = domain.lastIndexOf('.');
        if (tldIdx == -1) return false;

        var tld = domain.substring(tldIdx + 1);
        if (tld.isBlank()) return false;
        if (tld.length() == 2 || tld.length() == 3) return true;

        return false;
    }
}
