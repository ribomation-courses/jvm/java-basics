package ribomation;
import java.util.LinkedHashMap;
import java.util.Map;
import static ribomation.EmailChecker.isEmail;

public class App {
    public static void main(String[] args) {
        runTests();
    }

    static void runTests() {
        testData.forEach((input, output) -> {
            var passed = isEmail(input) == output;
            System.out.printf("%s: '%s' is %san email  %n",
                    passed ? "PASSED" : "FAILED",
                    input,
                    output ? "" : "not ");
        });
    }

    final static Map<String, Boolean> testData = new LinkedHashMap<>();
    static {
        testData.put(null, false);
        testData.put("", false);
        testData.put("   ", false);
        testData.put("@foo.bar", false);
        testData.put("   @foo.bar", false);
        testData.put("inge.vidare.foo.bar", false);
        testData.put("inge@vidare@foo.bar", false);
        testData.put("42.vidare@foo.bar", false);
        testData.put("inge.vidare.@fo#o.bar", false);
        testData.put("inge.v!da&e@ribomation.se", false);
        testData.put("inge.vidare@", false);
        testData.put("inge.vidare@42ribomation.se", false);
        testData.put("jens.riboe@ribomation.se", true);
        testData.put("jens.riboe@ribomation.info", false);
    }
}
