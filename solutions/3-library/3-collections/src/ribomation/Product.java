package ribomation;

import java.util.Objects;

public class Product implements Comparable<Product> {
    private String name;
    private float  price;
    private int    count;

    public Product(String name, float price, int count) {
        this.name = name;
        this.price = price;
        this.count = count;
    }

    public String getName() { return name; }
    public float getPrice() { return price; }
    public int getCount() { return count; }

    @Override
    public String toString() {
        return String.format("Product{%s, %.2f kr, %d left}", name, price, count);
    }

    @Override
    public boolean equals(Object that) {
        if (this.getClass() != that.getClass()) return false;
        return Objects.deepEquals(this, that);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, price, count);
    }

    @Override
    public int compareTo(Product o) {
        return this.name.compareTo(o.name);
    }
}
