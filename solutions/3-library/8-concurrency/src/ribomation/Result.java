package ribomation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class Result {
    long numDirs  = 0;
    long numFiles = 0;
    long numBytes = 0;

    public Result() {
    }

    void add(Path file) {
        ++numFiles;
        try {
            numBytes += Files.size(file);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    Result add(Result r) {
        numDirs += r.numDirs;
        numFiles += r.numFiles;
        numBytes += r.numBytes;
        return this;
    }

    @Override
    public String toString() {
        return String.format("# Dirs  = %,d%n# Files = %,d%n# Bytes = %,.3f MB",
                numDirs, numFiles, numBytes/ (1024 * 1024D));
    }
}
