package ribomation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ComputeTask extends RecursiveTask<Result> {
    private Path dir;

    public ComputeTask(Path dir) {
        if (!Files.isDirectory(dir)) {
            throw new IllegalArgumentException("not a directory: " + dir);
        }
        this.dir = dir;
    }

    @Override
    protected Result compute() {
        Result            result = new Result();
        List<ComputeTask> tasks  = new ArrayList<>();

        entries(dir).forEach(p -> {
            if (Files.isRegularFile(p)) {
                result.add(p);
            } else if (Files.isDirectory(p)) {
                result.numDirs++;
                tasks.add((ComputeTask) new ComputeTask(p).fork());
            }
        });

        return tasks.stream()
                .map(ForkJoinTask::join)
                .collect(() -> result, Result::add, Result::add);
    }

    Stream<Path> entries(Path dir) {
        try {
            return Files.list(dir);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
