package ribomation;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class ProductRepo {
    private List<Product> products = new ArrayList<>();

    public List<Product> getProducts() { return products; }

    ProductRepo load(String filename) throws IOException {
        products = new ArrayList<>();
        Files.readAllLines(Path.of(filename)).forEach(csv -> {
            if (csv.startsWith("name;")) return;
            String[] fields = csv.split(";");
            products.add(new Product(fields[0], fields[1], fields[2]));
        });
        return this;
    }
}
