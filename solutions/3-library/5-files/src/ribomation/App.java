package ribomation;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import static java.nio.file.Files.*;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class App {
    public static void main(String[] args) throws Exception {
        var app      = new App();
        app.run("./data/products.csv");
    }

    void run(String file) throws IOException {
        var products = new ProductRepo().load(file).getProducts();
        print(5, products);
        var p = store(products);
        printSize(p);
        copyFile(p);
        rename(p);
        deleteFiles(p);
    }

    void print(int lines, List<Product> products) {
        for (var p : products) {
            System.out.println(p);
            if (--lines <= 0) {
                System.out.println("...");
                return;
            }
        }
    }

    Path store(List<Product> products) throws IOException {
        var dir     = createTempDirectory("products");
        var outFile = Path.of(dir.toString(), "products.txt");
        writeString(outFile, products.toString());
        System.out.printf("written file %s%n", outFile);
        return outFile;
    }

    void printSize(Path file) throws IOException {
        System.out.printf("%s: %.3f KBytes%n", file, size(file) / 1024.0);
    }

    void copyFile(Path src) throws IOException {
        var dst = src.getParent().resolve("copy-products.txt");
        copy(src, dst, REPLACE_EXISTING);
        System.out.printf("written file %s%n", dst);
    }

    void rename(Path src) throws IOException {
        var dst = src.getParent().resolve("move-products.txt");
        move(src, dst, REPLACE_EXISTING);
        System.out.printf("written file %s%n", dst);
    }

    void deleteFiles(Path file) throws IOException {
        var dir   = file.getParent();
        for (var f : Objects.requireNonNull(dir.toFile().listFiles(File::isFile))) {
            deleteIfExists(f.toPath());
            System.out.printf("deleted file %s%n", f);
        }
        delete(dir);
        System.out.printf("deleted dir %s%n", dir);
    }

}
