package ribomation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

import static java.time.temporal.TemporalAdjusters.lastDayOfMonth;
import static java.util.Calendar.*;

public class LastDayOfMonth {
    public static void main(String[] args) {
        var app = new LastDayOfMonth();
        app.run();
    }

    String[] dates = {
            "2020-02-15", "2015-02-01", "", null
    };

    void run() {
        System.out.println("-- new API --");
        for (var d : dates) usingNewAPI(d);
        System.out.println("-- old API --");
        for (var d : dates) usingOldAPI(d);
    }

    void usingNewAPI(String date) {
        var lastDay = (isBlank(date) ? LocalDate.now() : LocalDate.parse(date))
                        .with(lastDayOfMonth());
        System.out.printf("'%s' --> %s%n", date, lastDay);
    }

    void usingOldAPI(String date) {
        final var fmt = new SimpleDateFormat("yyyy-MM-dd");
        try {
            var cal = Calendar.getInstance();
            cal.setTime(isBlank(date) ? new Date() : fmt.parse(date));
            cal.set(DAY_OF_MONTH, cal.getActualMaximum(DAY_OF_MONTH));
            cal.set(HOUR_OF_DAY, 0);
            cal.set(MINUTE, 0);
            cal.set(SECOND, 0);
            cal.set(MILLISECOND, 0);

            var lastDay = cal.getTime();
            System.out.printf("'%s' --> %s%n", date, fmt.format(lastDay));
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    boolean isBlank(String txt) {
        return txt == null || txt.isBlank();
    }
}
