import java.util.Scanner;

public class Stats {
    public static void main(String[] args) {
        var numbers = new int[5];
        var in = new Scanner(System.in);
        for (var k=0; k< numbers.length; ++k) {
            System.out.printf("%d(%d)? ", k+1, numbers.length);
            var n = in.nextInt();
            numbers[k] = n;
        }

        for (var n : numbers) System.out.printf("%d ", n);
        System.out.println();

        var sum = 0;
        {
            for (var n : numbers) sum += n;
            System.out.printf("sum=%d%n", sum);
        }

        {
            var prod = 1;
            for (var n : numbers) prod *= n;
            System.out.printf("prod=%d%n", prod);
        }

        {
            var min = Integer.MAX_VALUE;
            for (var n : numbers) min = n < min ? n : min;
            System.out.printf("min=%d%n", min);
        }

        {
            var max = Integer.MIN_VALUE;
            for (var n : numbers) max = n > max ? n : max;
            System.out.printf("max=%d%n", max);
        }

        System.out.printf("avg=%d%n", sum / numbers.length);
    }
}