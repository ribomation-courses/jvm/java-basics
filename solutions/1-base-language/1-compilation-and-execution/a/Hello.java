public class Hello {
    public static void main(String[] args) {
        System.out.printf("hi there: answer=%d%n", 42);

        String s1 = "hej";
        String s2 = "h" + "e" + "j";
        if (s1 == s2) System.out.println("s1 == s2");
        if (s1.equals(s2)) System.out.println("s1 equals s2");
    }
}