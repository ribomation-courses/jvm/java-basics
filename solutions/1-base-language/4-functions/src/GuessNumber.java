import java.util.Random;
import java.util.Scanner;

public class GuessNumber {
    public static void main(String[] args) {
        var app = new GuessNumber();
        app.init();
        app.run();
        app.print();
    }

    final int maxGuesses = 5;
    int numGuesses = 0;
    int secret;

    void init() {
        secret = new Random().nextInt(10) + 1;
    }

    void run() {
        for (numGuesses = 1; numGuesses <= maxGuesses; ++numGuesses) {
            var guess = readGuess();
            if (guess == secret) {
                System.out.println("!! correct !!");
                return;
            }
            if (guess < secret) System.out.println("too low");
            else System.out.println("too high");
        }
        System.out.println("** game over **");
    }

    void print() {
        System.out.printf("secret     : %d%n", secret);
        System.out.printf("num guesses: %d%n", numGuesses);
    }

    int readGuess() {
        System.out.print("Make a guess: ");
        return new Scanner(System.in).nextInt();
    }

}