public class Sum {
    public static void main(String[] args) {
        var N   = args.length == 0 ? 100 : Integer.parseInt(args[0]);
        var sum = N * (N + 1) / 2;
        System.out.printf("SUM(1..%d) = %d%n", N, sum);
    }
}