package ribomation;

import java.util.Locale;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.run(40, 2);
        System.out.println("----");
        app.run(29, 17);
    }

     void run(long lhs, long rhs) {
        for (var op : Operators.values()) print(op, lhs, rhs);
    }

     void print(Operators op, long lhs, long rhs) {
        System.out.printf(Locale.ENGLISH,
                "%d %s %d = %.3f%n",
                lhs, op, rhs, op.eval(lhs, rhs));
    }
}
