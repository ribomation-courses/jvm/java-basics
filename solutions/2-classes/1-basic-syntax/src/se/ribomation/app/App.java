package se.ribomation.app;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run();
    }

    private void run() {
        System.out.printf("hi there%n");

        var car = new Car("ABC123", "Volvo Amazon");
        System.out.println("car = " + car);

        var pers = new Person("Nisse");
        System.out.println("pers = " + pers);

        pers.setMyCar(car);
        System.out.println("pers = " + pers);
    }
}
