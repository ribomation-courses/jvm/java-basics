package se.ribomation.app;

public class Car {
    private String license;
    private String model;

    public Car(String license, String model) {
        this.license = license;
        this.model = model;
    }

    @Override
    public String toString() {
        return "Car{" +
                "license='" + license + '\'' +
                ", model='" + model + '\'' +
                '}';
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
