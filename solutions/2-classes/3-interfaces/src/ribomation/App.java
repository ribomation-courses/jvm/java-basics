package ribomation;
import java.util.Comparator;
import java.util.Random;
import static java.util.Arrays.sort;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run();
    }

    void run() {
        Shape[] shapes = {
                new Rect(val(), val()),
                new Circle(val()),
                new Rect(val(), val()),
                new Triangle(val(), val()),
                new Circle(val()),
                new Triangle(val(), val()),
        };
        for (var s : shapes) System.out.println(s);

        System.out.println("-------------");
        sort(shapes, new byArea());
        for (var s : shapes) System.out.println(s);

        System.out.println("-------------");
        sort(shapes, new byArea().reversed());
        for (var s : shapes) System.out.println(s);
    }

    static class byArea implements Comparator<Shape> {
        @Override
        public int compare(Shape lhs, Shape rhs) {
            return Double.compare(lhs.area(), rhs.area());
        }
    }

    int val() {
        return 1 + r.nextInt(10);
    }

    Random r = new Random();
}
