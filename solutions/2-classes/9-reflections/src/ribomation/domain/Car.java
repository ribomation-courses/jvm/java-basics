package ribomation.domain;

import java.util.StringJoiner;

public class Car extends Vehicle {
    private boolean turbo = true;

    public Car(String license) {
        super(license);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Car.class.getSimpleName() + "[", "]")
                .add("turbo=" + turbo)
                .toString();
    }

    public boolean isTurbo() {
        return turbo;
    }

    public void setTurbo(boolean turbo) {
        this.turbo = turbo;
    }
}
