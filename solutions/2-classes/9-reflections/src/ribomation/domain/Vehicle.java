package ribomation.domain;

import java.util.StringJoiner;

public class Vehicle {
    private String license;
    private Person owner;

    public Vehicle(String license) {
        this.license = license;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Vehicle.class.getSimpleName() + "[", "]")
                .add("license='" + license + "'")
                .add("owner=" + owner)
                .toString();
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }
}
