package ribomation;

import ribomation.domain.Car;
import ribomation.domain.Person;
import ribomation.domain.Vehicle;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.run();
    }

    void run() {
        var anna = new Person("Anna", 42, true);
        spy(anna);

        System.out.println("----");
        var volvo = new Car("ABC123");
        volvo.setOwner(anna);
        spy(volvo);
    }

    void spy(Object obj) {
        spy(obj.getClass());
    }

    void spy(Class cls) {
        System.out.printf("obj.fullClass = %s%n", cls.getName());
        System.out.printf("obj.class = %s%n", cls.getSimpleName());
        System.out.printf("obj.superClass = %s%n", cls.getSuperclass().getName());

        for (var m : cls.getDeclaredMethods()) {
            System.out.printf("obj.method: %s%n", m.getName());
        }

        for (var f : cls.getDeclaredFields()) {
            System.out.printf("obj.field: %s %s%n", f.getType(), f.getName());
        }

        if (cls.getSuperclass() != Object.class) {
            spy(cls.getSuperclass());
        }
    }

}
