
public class ExceptionDemo {
    public static void main(String[] args) {
        var app = new ExceptionDemo();
//        app.run1();
        app.run2();
    }

    void run1() {
        System.out.println("ExceptionDemo.app");
        fun1();
    }

    void run2() {
        System.out.println("ExceptionDemo.app");
        try {
            fun1();
        } catch (Exception e) {
            System.out.printf("Failure: %s%n", e);
            var location = e.getStackTrace()[0];
            System.out.printf("at: %s:%d in %s[%s] %n",
                    location.getFileName(),
                    location.getLineNumber(),
                    location.getClassName(),
                    location.getMethodName()
            );
        } finally {
            System.out.println("done");
        }
    }

    void fun1() {
        System.out.println("ExceptionDemo.fun1");
        fun2();
    }

    void fun2() {
        System.out.println("ExceptionDemo.fun2");
        fun3();
    }

    void fun3() {
        System.out.println("ExceptionDemo.fun3");
        Object obj = null;
        System.out.println(obj.toString());
    }
}