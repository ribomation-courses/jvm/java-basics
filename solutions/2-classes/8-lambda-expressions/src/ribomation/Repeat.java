package ribomation;
import java.util.ArrayList;
import java.util.List;

public class Repeat {
    interface RepeatHandler {
        void invoke(int k);
    }

    static <T> void repeat(final int n, RepeatHandler h) {
        for (var k = 1; k <= n; ++k) h.invoke(k);
    }

    public static void main(String[] args) {
        repeat(5, x -> System.out.printf("(%d) Java is cool%n", x));

        final List<Integer> lst = new ArrayList<>();
        repeat(10, lst::add);
        System.out.printf("lst: %s%n", lst);
    }

}
