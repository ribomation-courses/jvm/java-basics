package ribomation.domain;

import ribomation.nano_spring.Inject;
import ribomation.nano_spring.Bean;
import java.util.Objects;

@Bean
public class Car {
    private String model;

    @Inject private Engine engine;

    public Car(String model) {
        this.model = model;
    }

    @Override
    public String toString() {
        return String.format("Car{%s, %s}", model, engine);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Objects.equals(model, car.model);
    }

    @Override
    public int hashCode() {
        return Objects.hash(model);
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
