package ribomation;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.useCase1();
        app.useCase2();
        app.useCase3();
        app.useCase4();
    }

    void useCase1() {
        System.out.println("-- Integer --");
        var q = new SimpleQueue<Integer>(5);
        for (var k = 1; !q.full(); ++k) q.put(k);
        drain(q);
    }

    void useCase2() {
        System.out.println("-- String --");
        var q = new SimpleQueue<String>(5);
        for (var k = 1; !q.full(); ++k) q.put("Nisse-" + k * 10);
        drain(q);
    }

    void useCase3() {
        System.out.println("-- Double --");
        var q = new SimpleQueue<Double>(5);
        for (var k = 1; !q.full(); ++k) q.put(Math.PI + k * 100);
        drain(q);
    }

    void useCase4() {
        System.out.println("-- LocalDate --");
        var q = new SimpleQueue<LocalDate>(5);
        for (var k = 1; !q.full(); ++k) q.put(LocalDate.now().plus(k, ChronoUnit.DAYS));
        drain(q);
    }

    <T> void drain(SimpleQueue<T> q) {
        while (!q.empty()) System.out.printf("q: %s%n", q.get());
    }

}
