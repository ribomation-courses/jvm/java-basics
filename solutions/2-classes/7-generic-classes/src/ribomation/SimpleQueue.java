package ribomation;

public class SimpleQueue<T> {
    private final T[] payload;
    private       int size     = 0;
    private       int putIndex = 0;
    private       int getIndex = 0;

    public SimpleQueue(int capacity) {
        payload = (T[]) new Object[capacity];
    }

    public boolean full() {
        return size == payload.length;
    }
    public boolean empty() {
        return size == 0;
    }

    public T get() {
        try {
            return payload[getIndex++];
        } finally {
            --size;
            if (getIndex >= payload.length) getIndex = 0;
        }
    }

    public void put(T x) {
        try {
            payload[putIndex++] = x;
        } finally {
            ++size;
            if (putIndex >= payload.length) putIndex = 0;
        }
    }

}
