package se.ribomation.products.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Factory that can generate one or more products with fake data.
 */
public class ProductRepo {
    private Random r = new Random();

    /**
     * Generate a single product
     * @return a product
     */
    public Product create() {
        String name  = names[r.nextInt(names.length)];
        float  price = Math.abs((float) (r.nextGaussian() * 100 + 50));
        int    count = r.nextInt(10);
        return new Product(name, price, count);
    }

    /**
     * Generate a list of products
     * @param n how many to generate
     * @return list of products
     */
    public List<Product> create(int n) {
        var result = new ArrayList<Product>(n);
        for (var k = 0; k < n; ++k) result.add(create());
        return result;
    }

    private static final String[] names = {
            "apple", "banana", "coco nut", "date plum", "kiwi", "orange", "peach"
    };
}
