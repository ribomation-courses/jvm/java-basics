#!/usr/bin/env bash
set -eux

SCRIPT_DIR=`dirname $0`
. "${SCRIPT_DIR}/_settings.sh"

java -jar ${JAR_FILE}
