
SOURCE_DIR="./src"
JAVA_DIR="${SOURCE_DIR}/java"
META_DIR="${SOURCE_DIR}/meta"
MF_FILE="${META_DIR}/MANIFEST.MF"

BUILD_DIR="./bld"
CLASS_DIR="${BUILD_DIR}/classes"
JAR_DIR="${BUILD_DIR}/jars"
API_DIR="${BUILD_DIR}/api"

JAR_FILE="${JAR_DIR}/products.jar"
APP_PACKAGE="se.ribomation.products se.ribomation.products.domain"
