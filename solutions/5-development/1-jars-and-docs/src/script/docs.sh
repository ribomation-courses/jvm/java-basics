#!/usr/bin/env bash
set -eux

SCRIPT_DIR=`dirname $0`
. "${SCRIPT_DIR}/_settings.sh"

rm -rf ${API_DIR}
mkdir -p ${API_DIR}
javadoc -d ${API_DIR} -html5 -windowtitle 'Products App' --frames -private -sourcepath ${JAVA_DIR} ${APP_PACKAGE}
