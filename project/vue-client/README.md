# Vue.js REST-WS Client

This is a small demo connecting to a Java REST-WS server.

## Technologies Used

* Vue.js - SPA EcmaScript framework
* Vuitify - UI widget library for Vue
* Axios - Promise/async based REST invocation library

## Project setup

    npm install

## Build & Run

Ensure the server is started first

    npm run build
    npm run run:prod


## Compile and hot-reload for development

    npm run run:dev



