import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {path: '/home', component: () => import(/* webpackChunkName: "home" */ '../pages/home.page')},
    {path: '/person-list', component: () => import(/* webpackChunkName: "person-list" */ '../pages/person-list.page')},
    {path: '/person-view/:id', component: () => import(/* webpackChunkName: "person-view" */ '../pages/person-view.page')},

    {path: '/', redirect: '/home'},
    {path: '*', component: () => import(/* webpackChunkName: "not-found" */ '../pages/not-found.page')},
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
