package ribomation.server;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ribomation.database.DataBase;
import ribomation.database.DbPersonDAO;
import ribomation.domain.PersonDAO;
import ribomation.domain.PersonRepo;

import java.util.Map;

import static spark.Spark.*;

public class App {
    static final String JSON = "application/json";
    final        Logger log;
    final        int    port = 8000;
    PersonDAO repo;
    Gson      gson;

    public static void main(String[] args) {
        var app = new App();
        app.initDb();
        app.initSrv();
        app.routes();
        app.print();
    }

    public App() {
        this.log = LoggerFactory.getLogger(getClass());
    }

    void initDb() {
        var db = new DataBase("/datasource.properties");
        repo = new DbPersonDAO(db, true);

        if (repo.count() == 0) {
            var csvRepo = new PersonRepo("./src/main/resources/persons.csv");
            csvRepo.findAll().forEach(p -> repo.insert(p));
        }

        log.info("loaded {} objects", repo.findAll().size());
        // repo.findAll().forEach(p -> log.info(p.toString()));
    }

    void initSrv() {
        gson = new GsonBuilder()
                .setPrettyPrinting()
                .setDateFormat("yyyy-MM-dd HH:mm")
                .create();
        port(port);

        after((req, res) -> {
            res.type(JSON);
            if (!req.requestMethod().equals("OPTIONS")) {
                res.header("Access-Control-Allow-Origin", "*");
            }
        });

        notFound((req, res) -> {
//            res.header("Access-Control-Allow-Origin", "*");
            res.status(404);
            res.type(JSON);
            return gson.toJson(Map.of("message", "not found"));
        });
    }

    void routes() {
        get("/", (req, res) -> String.format("Server, version %d", 42));

        var personApi = new PersonController(repo, gson);

        path("/api/v1", () -> {
            before("/*", (req, res) -> log.info("{} {}", req.requestMethod(), req.uri()));

            path("/person", () -> {
                get("", personApi.getAll, src -> gson.toJson(src));
                post("", personApi.create, gson::toJson);
                get("/:id", personApi.getOne, gson::toJson);
                put("/:id", personApi.update, gson::toJson);
                delete("/:id", personApi.delete);

                options("", (req, res) -> {
                    res.header("Access-Control-Allow-Origin", "*");
                    res.header("Access-Control-Allow-Methods", "POST");
                    res.header("Access-Control-Allow-Headers", "Content-Type, Content-Length");
                    res.status(200);
                    return "";
                }, gson::toJson);

                options("/:id", (req, res) -> {
                    res.header("Access-Control-Allow-Origin", "*");
                    res.header("Access-Control-Allow-Methods", "PUT, DELETE");
                    res.header("Access-Control-Allow-Headers", "Content-Type, Content-Length");
                    res.status(200);
                    return "";
                }, gson::toJson);
            });
        });
    }

    void print() {
        log.info("server started http://localhost:{}/api/v1/person", port);
    }

}
