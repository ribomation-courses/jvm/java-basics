package ribomation.server;

import com.google.gson.Gson;
import ribomation.domain.Person;
import ribomation.domain.PersonDAO;
import spark.Route;

import java.util.Map;

import static spark.Spark.halt;


public class PersonController {
    private PersonDAO repo;
    private Gson      gson;

    public PersonController(PersonDAO repo, Gson gson) {
        this.repo = repo;
        this.gson = gson;
    }

    public Route getAll = (req, res) -> repo.findAll();

    public Route create = (req, res) -> {
        var p = gson.fromJson(req.body(), Person.class);
        if (p == null) {
            res.status(400);
            return "";
        }

        res.status(201);
        return repo.insert(p);
    };

    public Route getOne = (req, res) -> {
        var id = Integer.parseInt(req.params("id"));
        var p  = repo.findById(id);
        if (p.isEmpty()) {
            res.status(404);
            return Map.of("message", "not found: id=" + id);
        }

        return p.get();
    };

    public Route delete = (req, res) -> {
        var id = Integer.parseInt(req.params("id"));
        var p  = repo.findById(id);
        if (p.isEmpty()) {
            res.status(404);
            return Map.of("message", "not found: id=" + id);
        }

        repo.delete(id);
        res.status(204);
        return "";
    };

    public Route update = (req, res) -> {
        var id  = Integer.parseInt(req.params("id"));
        var obj = repo.findById(id);
        if (obj.isEmpty()) {
            res.status(404);
            return Map.of("message", "not found: id=" + id);
        }

        return repo.update(id, gson.fromJson(req.body(), Person.class));
    };

}
