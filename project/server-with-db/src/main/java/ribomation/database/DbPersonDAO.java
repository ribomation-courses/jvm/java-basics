package ribomation.database;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ribomation.domain.Person;
import ribomation.domain.PersonDAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import static java.lang.String.format;

public class DbPersonDAO implements PersonDAO {
    private final String   table   = "persons";
    private final Logger   log;
    private       DataBase db;
    private       boolean  verbose = false;

    public DbPersonDAO(DataBase db) {
        this(db, false);
    }

    public DbPersonDAO(DataBase db, boolean verbose) {
        this.db = db;
        this.verbose = verbose;
        this.log = LoggerFactory.getLogger(getClass());
    }

    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    @Override
    public Optional<Person> findById(int id) {
        var con = db.getConnection();
        try (con) {
            var stmt = con.createStatement();
            try (stmt) {
                var sql = format("SELECT * FROM %s WHERE id = %d", table, id);
                if (verbose) log.info(sql);
                var rs = stmt.executeQuery(sql);
                try (rs) {
                    if (rs.next()) {
                        return toPerson(rs);
                    }
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return Optional.empty();
    }

    @Override
    public void delete(int id) {
        var con = db.getConnection();
        try (con) {
            var stmt = con.createStatement();
            try (stmt) {
                var sql = format("DELETE FROM %s WHERE id = %d", table, id);
                if (verbose) log.info(sql);
                var rows = stmt.executeUpdate(sql);
                if (rows != 1) {
                    throw new RuntimeException("delete failed");
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Person update(int id, Person delta) {
        var con = db.getConnection();
        try (con) {
            var stmt = con.createStatement();
            try (stmt) {
                var buf = new StringBuilder();
                if (delta.getName() != null) {
                    if (buf.length() > 0) buf.append(", ");
                    buf.append("name").append(" = '").append(delta.getName()).append("'");
                }
                if (delta.getAge() > 0) {
                    if (buf.length() > 0) buf.append(", ");
                    buf.append("age").append(" = ").append(delta.getAge());
                }
                if (delta.getPostCode() > 0) {
                    if (buf.length() > 0) buf.append(", ");
                    buf.append("postCode").append(" = ").append(delta.getPostCode());
                }
                {
                    if (buf.length() > 0) buf.append(", ");
                    buf.append("female").append(" = ").append(delta.isFemale() ? 1 : 0);
                }
                
                var sql = format("UPDATE %s SET %s WHERE id = %d;",
                        table, buf.toString(), id);
                if (verbose) log.info(sql);
                var rows = stmt.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
                if (rows != 1) {
                    throw new RuntimeException("update failed");
                }

                return findById(id).get();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Person insert(Person p) {
        var con = db.getConnection();
        try (con) {
            var stmt = con.createStatement();
            try (stmt) {
                var sql = format("INSERT INTO %s (name,female,age,postCode) VALUES ('%s', %d, %d, %d);",
                        table, p.getName(), p.isFemale() ? 1 : 0, p.getAge(), p.getPostCode());
                if (verbose) log.info(sql);
                var rows = stmt.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
                if (rows == 0) {
                    throw new RuntimeException("insert failed");
                }

                //https://stackoverflow.com/questions/1915166/how-to-get-the-insert-id-in-jdbc
                var rs = stmt.getGeneratedKeys();
                try (rs) {
                    if (rs.next()) {
                        p.setId(rs.getInt(1));
                        return p;
                    } else {
                        throw new RuntimeException("cannot fetch new/generated primary key");
                    }
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Collection<Person> findAll() {
        var con = db.getConnection();
        try (con) {
            var stmt = con.createStatement();
            try (stmt) {
                var sql = format("SELECT * FROM %s", table);
                if (verbose) log.info(sql);
                var rs = stmt.executeQuery(sql);
                try (rs) {
                    var result = new ArrayList<Person>();
                    while (rs.next()) {
                        toPerson(rs).ifPresent(result::add);
                    }
                    return result;
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public int count() {
        var con = db.getConnection();
        try (con) {
            var stmt = con.createStatement();
            try (stmt) {
                var sql = format("SELECT count(*) AS count FROM %s", table);
                if (verbose) log.info(sql);
                var rs = stmt.executeQuery(sql);
                try (rs) {
                    if (rs.next()) {
                        return rs.getInt("count");
                    }
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    private Optional<Person> toPerson(ResultSet rs) throws SQLException {
        var pk       = rs.getInt("id");
        var name     = rs.getString("name");
        var age      = rs.getInt("age");
        var postCode = rs.getInt("postCode");
        var female   = rs.getInt("female") == 1;

        return Optional.of(new Person(pk, name, female, age, postCode));
    }

}
