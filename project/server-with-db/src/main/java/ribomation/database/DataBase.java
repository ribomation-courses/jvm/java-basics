package ribomation.database;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DataBase implements Closeable {
    private Properties dbProps = new Properties();
    private Connection con;

    public DataBase(String resourcePath) {
        var is = getClass().getResourceAsStream(resourcePath);
        if (is == null) {
            throw new RuntimeException("cannot open db resource path: " + resourcePath);
        }
        try {
            dbProps.load(is);
            is.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Connection getConnection() {
        try {
            if (con == null || con.isClosed()) {
                return open();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return con;
    }

    public Connection open() {
        try {
            return con = DriverManager.getConnection(dbProps.getProperty("url"), dbProps);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
