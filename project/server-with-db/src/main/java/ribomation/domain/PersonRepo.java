package ribomation.domain;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

import static java.nio.file.Files.isReadable;
import static java.util.stream.Collectors.toList;

public class PersonRepo implements PersonDAO {
    private List<Person> db = new ArrayList<>();

    public PersonRepo(String filename) {
        try {
            load(filename);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void load(String filename) throws Exception {
        var path = Path.of(filename);
        if (!isReadable(path)) {
            throw new RuntimeException("cannot open " + filename);
        }

        this.db =
                Files.lines(path)
                        .skip(1)
                        .map(line -> line.split(","))
                        .map(arr -> new Person(arr[0], arr[1], arr[2], arr[3]))
                        .collect(toList())
        ;
    }

    @Override
    public int count() {
        return db.size();
    }

    @Override
    public Collection<Person> findAll() {
        return db;
    }

    @Override
    public Optional<Person> findById(int id) {
        return db.stream().filter(p -> p.getId() == id).findFirst();
    }

    @Override
    public Person insert(Person p) {
        var max = db.stream().mapToInt(Person::getId).max();
        p.setId(max.getAsInt() + 1);
        db.add(p);
        return p;
    }

    @Override
    public void delete(int id) {
        db.removeIf(p -> p.getId() == id);
    }

    @Override
    public Person update(int id, Person P) {
        var obj = findById(id);
        if (obj.isPresent()) {
            var p = obj.get();
            if (P.getName() != null) p.setName(P.getName());
            if (P.getAge() > 0) p.setAge(P.getAge());
            if (P.isFemale() != p.isFemale()) p.setFemale(P.isFemale());
            return p;
        }
        return null;
    }

}
