package ribomation.domain;

import java.util.Collection;
import java.util.Optional;

public interface PersonDAO {
    Collection<Person> findAll();

    Optional<Person> findById(int id);

    Person insert(Person p);

    void delete(int id);

    Person update(int id, Person delta);

    default int count() { return 0; }

}
