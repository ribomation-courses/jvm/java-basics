package ribomation.server;

import com.google.gson.Gson;
import ribomation.domain.Person;
import ribomation.domain.PersonDAO;
import spark.Route;

import static spark.Spark.halt;


public class PersonController {
    private PersonDAO repo;
    private Gson      gson;

    public PersonController(PersonDAO repo, Gson gson) {
        this.repo = repo;
        this.gson = gson;
    }

    public Route getAll = (req, res) -> repo.findAll();

    public Route create = (req, res) -> {
        var p = gson.fromJson(req.body(), Person.class);
        if (p == null) halt(400);
        res.status(201);
        return repo.insert(p);
    };

    public Route getOne = (req, res) -> {
        var id = Integer.parseInt(req.params("id"));
        var p  = repo.findById(id);
        if (p.isEmpty()) halt(404);

        return p.get();
    };

    public Route delete = (req, res) -> {
        var id = Integer.parseInt(req.params("id"));
        var p  = repo.findById(id);
        if (p.isEmpty()) halt(404);

        repo.delete(id);
        res.status(204);
        return "";
    };

    public Route update = (req, res) -> {
        var id  = Integer.parseInt(req.params("id"));
        var obj = repo.findById(id);
        if (obj.isEmpty()) halt(404);

        var P = gson.fromJson(req.body(), Person.class);
        var p = obj.get();
        if (P.getName() != null) p.setName(P.getName());
        if (P.getAge() > 0) p.setAge(P.getAge());
        if (P.isFemale() != p.isFemale()) p.setFemale(P.isFemale());

        return p;
    };

}
