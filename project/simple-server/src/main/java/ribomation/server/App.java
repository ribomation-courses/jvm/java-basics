package ribomation.server;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import ribomation.domain.PersonDAO;
import ribomation.domain.PersonRepo;

import java.util.Map;

import static spark.Spark.*;

public class App {
    static final String JSON = "application/json";
    PersonDAO repo;
    Gson      gson;

    public static void main(String[] args) {
        var app = new App();
        app.initDb();
        app.initSrv();
        app.routes();
        app.print();
    }

    void initDb() {
        repo = new PersonRepo("./src/main/resources/persons.csv");
        System.out.printf("loaded %d objects%n", repo.findAll().size());
        //repo.findAll().forEach(System.out::println);
    }

    void initSrv() {
        gson = new GsonBuilder()
                .setPrettyPrinting()
                .setDateFormat("yyyy-MM-dd HH:mm")
                .create();
        port(8000);

        after((req, res) -> {
            res.type(JSON);
        });

        notFound((req, res) -> gson.toJson(Map.of("message", "not found")));
    }

    void routes() {
        get("/", (req, res) -> String.format("Server, version %d", 42));

        var personApi = new PersonController(repo, gson);

        path("/api/v1", () -> {
            before("/*", (req,res) -> System.out.printf("%s %s%n", req.requestMethod(), req.uri()));

            path("/person", () -> {
                get("", personApi.getAll, src -> gson.toJson(src));
                post("", personApi.create, gson::toJson);
                get("/:id", personApi.getOne, gson::toJson);
                put("/:id", personApi.update, gson::toJson);
                delete("/:id", personApi.delete);
            });
        });

    }

    void print() {
        System.out.printf("server started http://localhost:8000/%n%n");
    }

}
