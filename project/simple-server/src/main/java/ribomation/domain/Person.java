package ribomation.domain;

import java.util.Objects;
import java.util.StringJoiner;

public class Person implements Comparable<Person> {
    private static int    nextId = 1;
    private        int    id     = nextId++;
    private        String  name;
    private        boolean female;
    private        int     age;
    private        int     postCode;

    public Person(String name, String female, String age, String postCode) {
        this(name, female.equals("Female"), Integer.parseInt(age), Integer.parseInt(postCode));
    }

    public Person(String name, boolean female, int age, int postCode) {
        this.name = name;
        this.female = female;
        this.age = age;
        this.postCode = postCode;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Person.class.getSimpleName() + "[", "]")
                .add("id='" + id + "'")
                .add("name='" + name + "'")
                .add("female=" + female)
                .add("age=" + age)
                .add("postCode=" + postCode)
                .toString();
    }

    @Override
    public int compareTo(Person that) {
        return this.name.compareTo(that.name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return  getId() == person.getId() &&
                isFemale() == person.isFemale() &&
                getAge() == person.getAge() &&
                getPostCode() == person.getPostCode() &&
                getName().equals(person.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), isFemale(), getAge(), getPostCode());
    }

    public int getId() { return id; }

    public String getName() {
        return name;
    }

    public boolean isFemale() {
        return female;
    }

    public int getAge() {
        return age;
    }

    public int getPostCode() {
        return postCode;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFemale(boolean female) {
        this.female = female;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setPostCode(int postCode) {
        this.postCode = postCode;
    }
}
