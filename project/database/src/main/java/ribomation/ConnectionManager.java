package ribomation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {
    private String host = "localhost";
    private int    port = 3306;
    private String db   = "person_db";
    private String usr  = "root";
    private String pwd  = "admin";

    public Connection get() throws SQLException {
        return DriverManager.getConnection(jdbcUrl(), usr, pwd);
    }

    protected String jdbcUrl() {
        return String.format("jdbc:mysql://%s:%d/%s?serverTimezone=UTC", host, port, db);
    }

/*
    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
*/
}
