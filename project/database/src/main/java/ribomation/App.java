package ribomation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;

public class App {
    public static void main(String[] args) throws Exception {
        var app = new App();
        app.update(2, "city", "Stockholm");
        //app.create("Nisse", "Hult", "Stockholm");
        app.delete(12);
        app.delete(11);
        app.run();
    }

    void run() throws SQLException {
        var con = new ConnectionManager().get();
        try (con) {
            var stmt = con.createStatement();
            try (stmt) {
                var rs = stmt.executeQuery("SELECT * FROM profiles");
                try (rs) {
                    while (rs.next()) {
                        System.out.printf("row: (%d) %s %s, %s%n",
                                rs.getInt("id"),
                                rs.getString("first_name"),
                                rs.getString("last_name"),
                                rs.getString("city")
                        );
                    }
                }
            }
        }
    }

    void update(int id, String field, String value) throws SQLException {
        var con = new ConnectionManager().get();
        try (con) {
            var stmt = con.createStatement();
            try (stmt) {
                var sql = String.format("UPDATE profiles SET %s = '%s' WHERE id = %d", field, value, id);
                var res = stmt.executeUpdate(sql);
                System.out.printf("updated %d rows%n", res);
            }
        }
    }

    void create(String fname, String lname, String city) throws SQLException {
        var con = new ConnectionManager().get();
        try (con) {
            var stmt = con.createStatement();
            try (stmt) {
                var sql = String.format("INSERT INTO profiles (first_name, last_name, city) VALUES ('%s', '%s', '%s');",
                        fname, lname, city
                );
                var res = stmt.executeUpdate(sql);
                System.out.printf("updated %d rows%n", res);
            }
        }
    }

    void delete(int id) throws SQLException {
        var con = new ConnectionManager().get();
        try (con) {
            var stmt = con.createStatement();
            try (stmt) {
                var sql = String.format("DELETE FROM profiles WHERE id = %d", id);
                var res = stmt.executeUpdate(sql);
                System.out.printf("updated %d rows%n", res);
            }
        }
    }

}
