create table profiles (
	id INT PRIMARY KEY AUTO_INCREMENT,
	first_name VARCHAR(50),
	last_name VARCHAR(50),
	city VARCHAR(50),
	photo VARCHAR(50)
);
insert into profiles (id, first_name, last_name, city, photo) values (1, 'Joby', 'Crawshay', 'Magtangol', 'http://dummyimage.com/x.png/cc0000/ffffff');
insert into profiles (id, first_name, last_name, city, photo) values (2, 'Bradly', 'Castella', 'Fiães', 'http://dummyimage.com/x.png/cc0000/ffffff');
insert into profiles (id, first_name, last_name, city, photo) values (3, 'Minetta', 'Rutherfoord', 'Panambi', 'http://dummyimage.com/x.png/cc0000/ffffff');
insert into profiles (id, first_name, last_name, city, photo) values (4, 'Chick', 'Boobyer', 'Huanggong', 'http://dummyimage.com/x.png/5fa2dd/ffffff');
insert into profiles (id, first_name, last_name, city, photo) values (5, 'Ludvig', 'Randles', 'Amarillo', 'http://dummyimage.com/x.png/cc0000/ffffff');
insert into profiles (id, first_name, last_name, city, photo) values (6, 'Amara', 'Elcock', 'La Gloria', 'http://dummyimage.com/x.png/dddddd/000000');
insert into profiles (id, first_name, last_name, city, photo) values (7, 'Kailey', 'Grew', 'Nangerang', 'http://dummyimage.com/x.png/5fa2dd/ffffff');
insert into profiles (id, first_name, last_name, city, photo) values (8, 'Kelly', 'Mathivet', 'Ostrowite', 'http://dummyimage.com/x.png/cc0000/ffffff');
insert into profiles (id, first_name, last_name, city, photo) values (9, 'Iggy', 'Toplin', 'Bulusari', 'http://dummyimage.com/x.png/5fa2dd/ffffff');
insert into profiles (id, first_name, last_name, city, photo) values (10, 'Mitchael', 'Calles', 'Nîmes', 'http://dummyimage.com/x.png/cc0000/ffffff');
