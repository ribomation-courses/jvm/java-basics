# Java Programming Basics, 5 days

Welcome to this course.
The syllabus can be found at
[jvm/java-basics](https://www.ribomation.se/courses/jvm/java-basics)

Here you will find
* Installation instructions
* Solutions to the programming exercises
* Source code to demo projects

# Installation Instructions

The latest version of Java JDK installed. 
* [Java JDK Download](https://adoptopenjdk.net/)
  - N.B. This is not the ordinary Oracle download above, because starting with version 11, Oracle plan to charge for support of Java, which means you are better off going with OpenJDK in the first place. Go for the latest version and the HotSpot engine.

You also need a decent Java IDE, such as
* JetBrains IntellJ IDEA
    - https://www.jetbrains.com/idea/download
* MicroSoft Visual Code
    - https://code.visualstudio.com/


# Using this GIT Repo

You need to have a GIT client installed to clone this repo. 
Otherwise, you can just click on the download button and grab it all as a ZIP or TAR bundle.

* [GIT Client Download](https://git-scm.com/downloads)

Get the sources initially by a `git clone` operation

    mkdir -p ~/java-course/my-solutions
    cd ~/java-course
    git clone https://gitlab.com/ribomation-courses/jvm/java-basics.git gitlab

Get the latest updates by a `git pull` operation

    cd ~/java-course/gitlab
    git pull

***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>

